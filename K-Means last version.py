
import numpy as np


import matplotlib.pyplot as plt

from numpy.linalg import norm

n_clusters = 5
max_iter = 100
rseed = 6
row_norm = ""
X = [-10, -8, -6, -3.3, -0.1, 0,1.1,1.5,1.8,2.2,2.4,3.3,3.4,3.8,3.9,4.4,4.7,5.5,6.6,8,9,10,20,50]
centers = []


def initialize_center(X, n_clusters, rseed):
    rng = np.random.RandomState(rseed)
    i = rng.permutation(np.shape(X)[0])[:n_clusters]
    centers = np.array(i)
    return np.array(centers)


def add_records(centers, labels,n_clusters):           
    centroids = np.zeros((n_clusters,np.shape(X)[0]))
    for k in range(n_clusters):
        centroids[k] = np.mean(X[k])
    return centroids


def find_distance(centers, centroids,n_clusters):
    distance = np.zeros((np.shape(X)[0], n_clusters))
    for k in range(n_clusters):
        row_norm = norm(centers - centroids[k])
        distance[:, k] = np.square(row_norm)
    return distance


def find_closest_cluster(distance):
    return np.argmin(distance, axis = 1)


def compute_sse(centers, labels, centroids, n_clusters):
    distance = np.zeros(np.shape(X)[0])
    for k in n_clusters:
        distance[labels == k] = norm(X[labels == k] - centroids[k], axis = 1)
    return np.sum(np.square(distance))


def fit(centers, max_iter, labels, n_clusters):
    centroids = initialize_center(centers,n_clusters,rseed)
    
    for i in range(max_iter):
        old_centroids = centroids
        distance = find_distance(centroids, old_centroids, n_clusters)
        labels = find_closest_cluster(distance)
        centroids = add_records(centers, labels, n_clusters)
        if np.all(old_centroids == centroids):
            plt.scatter(X, X)
            plt.scatter(centroids[ :, 0], centroids[ :, 1])  
            plt.show()
            break
       

fit(X,max_iter,4,5)      